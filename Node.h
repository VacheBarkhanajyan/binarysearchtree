#ifndef NODE_H
#define NODE_H

template <typename T>
struct Node {
	T data;
	Node<T>* left;
	Node<T>* right;
	Node(const T& d, Node<T>* l = nullptr, Node<T>* r = nullptr)
		: data(d)
		, left(l)
		, right(r)
	{}
};

#endif