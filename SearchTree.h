#ifndef ST_H
#define ST_H

#include "BinaryTree.h"

template <typename T>
class BST : public Tree<T>
{
public:
	void insert(const T& data);
	void insertI(const T& data);

	bool contains(const T& data) const;
	

	const T& findMax() const;

	const T& findMinI() const;
	
	void remove(const T& data);

private:
	Node<T>* insertHelper(Node<T>* p, const T& data);

	const Node<T>* find(const Node<T>* p, const T& data) const;
	

	const Node<T>* findMaxNode(const Node<T>* p) const;
	const Node<T>* findMinNodeI(const Node<T>* p) const;

	Node<T>* removeHelper(Node<T>* n, const T& d);
};

#endif