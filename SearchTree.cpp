#include "BinaryTree.h"
#include "SearchTree.h"

template <typename T>
void BST<T>::insert(const T& data)
{
	this->m_root = insertHelper(this->m_root, data);
}

template <typename T>
Node<T>* BST<T>::insertHelper( Node<T>* p, const T& data)
{
	if (!p)
	{
		p = new Node<T>(data);
	}
	else if (p->data < data)
	{
		p->right = insertHelper(p->right, data);
	}
	else if (p->data > data)
	{
		p->left = insertHelper(p->left, data);
	}
	return p;
}

template <typename T>
void BST<T>::insertI(const T& data)
{
	if (!this->m_root) {
		this->m_root = new Node<T>(data);
		return;
	}

	Node<T>* parent = nullptr;
	Node<T>* temp = this->m_root;
	while (temp)
	{
		parent = temp;
		if (data > temp->data) {
			temp = temp->right;
		}
		else if (data < temp->data) {
			temp = temp->left;
		}
		else {
			std::cout << "Existing value: skipping... " << data << "\n";
			return;
		}		
	}

	if (data < parent->data) {
		parent->left = new Node<T>(data);
	}
	else {
		parent->right = new Node<T>(data);
	}
}

template<typename T>
bool BST<T>::contains(const T& data) const
{
	return find(this->m_root, data) != nullptr;
}

template <typename T>
const Node<T>* BST<T>::find(const Node<T>* p, const T& data) const
{
	if (p == nullptr)
		return nullptr;

	if (data > p->data)
	{
		return find(p->right, data);
	}
	if (data < p->data)
	{
		return find(p->left, data);
	}
	return p;
}

template<typename T>
const T& BST<T>::findMax() const
{
	return findMaxNode(this->m_root)->data;
}

template<typename T>
const Node <T>* BST<T>::findMaxNode(const Node <T>* p) const
{
	if (!p)
	{
		return nullptr;
	}
	if (!p->right)
	{
		return p;
	}
	return findMaxNode(p->right);
}

template <typename T>
const T& BST<T>::findMinI() const
{
	return findMinNodeI(this->m_root)->data;
}

template <typename T>
const Node<T>* BST<T>::findMinNodeI(const Node<T>* p) const
{
	while (p && p->left)
	{
		p = p->left;
	}
	return p;
}

template <typename T>
void BST<T>::remove(const T& data)
{
	this->m_root = removeHelper(this->m_root, data);
}

template <typename T>
Node<T>* BST<T>::removeHelper(Node<T>* n, const T& d)
{
	if (!n) return nullptr;

	if (d > n->data)
	{
		n->right = removeHelper(n->right, d);
	}
	else if (d < n->data)
	{
		n->left = removeHelper(n->left, d);
	}
	else 
	{
		if (!n->left && !n->right)
		{
			delete n;
			n = nullptr;
			//std::cout << "deleting leaf" << d << "\n";
			return nullptr;
		}
		if (!n->left)
		{
			//std::cout << "deleting left child" << d << "\n";
			Node<T>* tmp = n->right;
			delete n;
			return tmp;
		}
		if (!n->right)
		{
			//std::cout << "deleting right child" << d << "\n";
			Node<T>* tmp = n->left;
			delete n;
			return tmp;
		}
		const Node<T>* t = findMinNodeI(n->right);
		//std::cout << "min of maxes" << t->data << "\n";
		n->data = t->data;
		n->right = removeHelper(n->right, t->data);
	}
	return n;
}