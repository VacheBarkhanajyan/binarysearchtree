
#include "Node.h"
#include "SearchTree.h"
#include <stack>
#include <iostream>
#include <queue>

template <typename T>
Tree<T>::Tree(Node<T>* t)
	: m_root(t)
{}

template <typename T>
Tree<T>::~Tree()
{
	clear();
}

template <typename T>
void Tree<T>::preorderTraverse() const {
	preorderHelper(m_root);
}

template <typename T>
void Tree<T>::preorderHelper(const Node<T>* t) const {
	if (!t) {
		return;
	}
	std::cout << t->data << " ";
	preorderHelper(t->left);
	preorderHelper(t->right);
}

template <typename T>
void Tree<T>::inorderTraverse() const {
	inorderHelper(m_root);
}

template <typename T>
void Tree<T>::inorderHelper(const Node<T>* t) const {
	if (!t) {
		return;
	}
	inorderHelper(t->left);
	std::cout << t->data << " ";
	inorderHelper(t->right);
}

template <typename T>
void Tree<T>::postorderTraverse() const {
	postorderHelper(m_root);
}

template <typename T>
void Tree<T>::postorderHelper(const Node<T>* t) const {
	if (!t) {
		return;
	}
	postorderHelper(t->left);
	postorderHelper(t->right);
	std::cout << t->data << " ";
}


template <typename T>
void Tree<T>::preorderTraverseI() const
{
	std::stack<Node<T>*> s;
	Node<T>* temp = m_root;
	while (true)
	{
		while (temp)
		{
			std::cout << temp->data << " ";
			if (temp->right)
			{
				s.push(temp->right);

			}
			temp = temp->left;
		}
		if (s.empty())
		{
			return;
		}
		temp = s.top();
		s.pop();
	}
}

template <typename T>
void Tree<T>::inorderTraverseI() const
{
	std::stack<Node<T>*> s;
	Node<T>* temp = m_root;
	while (true)
	{
		while (temp)
		{
			s.push(temp);
			temp = temp->left;
		}
		if (s.empty())
		{
			return;
		}
		temp = s.top();
		s.pop();
		std::cout << temp->data << " ";
		temp = temp->right;
	}
}

template <typename T>
void Tree<T>::postorderTraverseI() const
{
	std::stack<Node<T>*> s;
	Node<T>* tmp = m_root;
	while (true)
	{
		while (tmp)
		{
			if (tmp->right)
			{
				s.push(tmp->right);
			}
			s.push(tmp);
			tmp = tmp->left;
		}
		if (s.empty()) return;

		tmp = s.top();
		s.pop();
		if (tmp->right && !s.empty() && tmp->right == s.top())
		{
			s.pop();
			s.push(tmp);
			tmp = tmp->right;
		}
		else
		{
			std::cout << tmp->data << " ";
			tmp = nullptr;
		}
	}
}

template<typename T>
void Tree<T>::levelorderTraverseI() const
{
	if (!m_root)
	{
		return;
	}

	std::queue<Node<T>*> q;

	q.push(m_root);
	while (!q.empty())
	{
		Node<T>* temp = q.front();
		q.pop();
		std::cout << temp->data << " ";

		if (temp->left)
		{
			q.push(temp->left);
		}

		if (temp->right)
		{
			q.push(temp->right);
		}
	}
}

template<typename T>
void Tree<T>::clearHelper(const Node<T>* p)
{
	if (!p)
	{
		return;
	}

	clearHelper(p->left);
	clearHelper(p->right);
	delete p;
	p = nullptr;
}

template<typename T>
void Tree<T>::clear()
{
	clearHelper(m_root);
	m_root = nullptr;
}

template <typename T>
int Tree<T>::width() const
{
	if (!m_root) return 0;

	int w = 1;
	std::queue<Node<T>*> q;
	q.push(m_root);

	while (!q.empty())
	{
		const int size = q.size();
		if (size > w) w = size;
		for (int i = 0; i < size; ++i)
		{
			Node<T>* tmp = q.front();
			q.pop();
			if (tmp->left) q.push(tmp->left);
			if (tmp->right) q.push(tmp->right);
		}
	}
	return w;
}

template <typename T>
int Tree<T>::height() const
{
	return heightHelper(m_root) - 1;
}

template <typename T>
int Tree<T>::heightHelper(const Node<T>* p) const
{
	if (!p) return 0;

	return 1 + std::max(heightHelper(p->left), heightHelper(p->right));
}

template <typename T>
int Tree<T>::countOfLeaves() const
{
	return countOfLeavesHelper(m_root);
}

template <typename T>
int Tree<T>::countOfLeavesHelper(const Node<T>* p) const
{
	if (!p) return 0;
	if (!p->left && !p->right) return 1;

	return countOfLeavesHelper(p->left) + countOfLeavesHelper(p->right);
}

template <typename T>
int Tree<T>::countOfNodes() const
{
	return countOfNodesHelper(m_root);
}

template <typename T>
int Tree<T>::countOfNodesHelper(const Node<T>* p) const
{
	if (!p) return 0;

	return 1 + countOfNodesHelper(p->left) + countOfNodesHelper(p->right);
}

template <typename T>
void Tree<T>::zigzagTraverse() const
{
	if (!m_root) return;
	std::queue<Node<T>*> q;
	q.push(m_root);
	bool flag = false;
	while (!q.empty())
	{
		const int size = q.size();
		if (flag)
		{
			std::stack<Node<T>*> s;
			for (int i = 0; i < size; ++i)
			{
				Node<T>* n = q.front();
				s.push(n);
				if (n->left) q.push(n->left);
				if (n->right) q.push(n->right);
				q.pop();
			}
			while (!s.empty())
			{
				std::cout << s.top()->data << " ";
				s.pop();
			}
			flag = !flag;
		}
		else
		{
			for (int i = 0; i < size; ++i)
			{
				Node<T>* n = q.front();
				std::cout << n->data << " ";
				if (n->left) q.push(n->left);
				if (n->right) q.push(n->right);
				q.pop();
			}
			flag = !flag;
		}
	}
}

template <typename T>
int Tree<T>::countOfNodesI() const
{
	if (!m_root)
	{
		return 0;
	}
	int count = 0;
	std::queue<Node<T>*> q;

	q.push(m_root);
	while (!q.empty())
	{
		Node<T>* temp = q.front();
		q.pop();

		++count;
		if (temp->left)
		{
			q.push(temp->left);
		}

		if (temp->right)
		{
			q.push(temp->right);
		}
	}
	return count;
}

template <typename T>
int Tree<T>::countOfLeavesI() const
{
	if (!m_root) return 0;
	int count = 0;
	std::queue<Node<T>*> q;

	q.push(m_root);
	while (!q.empty())
	{
		Node<T>* temp = q.front();
		q.pop();
		if (!temp->left && !temp->right)
		{
			++count;
		}
		else
		{
			if (temp->left)
			{
				q.push(temp->left);
			}

			if (temp->right)
			{
				q.push(temp->right);
			}
		}
	}
	return count;
}

template <typename T>
void Tree<T>::postOrderWithVisitor(void (*visit)(Node<T>* p))
{
	std::stack<Node<T>*> s;
	Node<T>* tmp = m_root;
	while (true)
	{
		while (tmp)
		{
			if (tmp->right)
			{
				s.push(tmp->right);
			}
			s.push(tmp);
			tmp = tmp->left;
		}
		if (s.empty()) return;

		tmp = s.top();
		s.pop();
		if (tmp->right && !s.empty() && tmp->right == s.top())
		{
			s.pop();
			s.push(tmp);
			tmp = tmp->right;
		}
		else
		{
			visit(tmp);
			tmp = nullptr;
		}

	}

}

template <typename T>
void Tree<T>::setNodeLeavesCountI()
{
	postOrderWithVisitor(f<T>);
}

template <typename T>
void  Tree<T>::setNodeValueToLC()
{
	setNodeValueToLCHelper(m_root);
}

template <typename T>
void  Tree<T>::setNodeValueToLCHelper(Node<T>* p)
{

	if (!p)
		return;
	if (!p->left && !p->right)
	{
		p->data = 1;
		return;
	}

	setNodeValueToLCHelper(p->left);
	setNodeValueToLCHelper(p->right);

	p->data = 0;
	if (p->left)
	{
		p->data += p->left->data;
	}

	if (p->right)
	{
		p->data += p->right->data;
	}

}