#ifndef BT_H
#define  BT_H

#include "Node.h"
#include <stack>
#include <iostream>
#include <queue>


template <typename T>
void f(Node<T>* tmp)
{
	if (!tmp->left && !tmp->right)
	{
		tmp->data = 1;
	}
	else
	{
		tmp->data = (tmp->left ? tmp->left->data : 0) + (tmp->right ? tmp->right->data : 0);
	}
}

template <typename T>
void printNode(Node<T>* n)
{
	std::cout << n->data << " ";
}

template <typename T>
class Tree {
public:
	Tree(Node<T>* t = nullptr);
	~Tree();

	void preorderTraverse() const;
	void postorderTraverse() const;
	void inorderTraverse() const;

	void preorderTraverseI() const;
	void inorderTraverseI() const;
	void postorderTraverseI() const;

	void levelorderTraverseI() const;

	void clear();

	int width() const;
	int height() const;
	int countOfLeaves() const;
	int countOfNodes() const;

	int countOfNodesI() const;
	int countOfLeavesI() const;

	void zigzagTraverse() const;
	void  setNodeValueToLC();
	void setNodeLeavesCountI();

	
	void postOrderWithVisitor(void (*visit)(Node<T>* p));

private:
	void preorderHelper(const Node<T>* p) const;
	void postorderHelper(const Node<T>* p) const;
	void inorderHelper(const Node<T>* p) const;

	void clearHelper(const Node<T>* p);

	int heightHelper(const Node<T>* p) const;
	int countOfLeavesHelper(const Node<T>* p) const;
	int countOfNodesHelper(const Node<T>* p) const;

	void  setNodeValueToLCHelper(Node<T>* p);

protected:
	Node<T>* m_root;
};




#endif