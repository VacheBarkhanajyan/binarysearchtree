#include <iostream>

#include "BinaryTree.h"
#include "SearchTree.h"
#include "Node.h"

int main() {
	/*Node<char>* i = new Node<char>('i');
	Node<char>* g = new Node<char>('g', i);
	Node<char>* d = new Node<char>('d', nullptr, g);
	Node<char>* e = new Node<char>('e');
	Node<char>* b = new Node<char>('b', d, e);

	Node<char>* h = new Node<char>('h');
	Node<char>* f = new Node<char>('f', nullptr, h);
	Node<char>* c = new Node<char>('c', f);

	Node<char>* a = new Node<char>('a', b, c);

	Tree<char> tree(a);*/

	BST<int> tree;
	tree.insert(5);
	tree.insert(47);
	tree.insert(66);
	tree.insert(4);
	tree.insert(7);
	tree.insert(5);

	tree.setNodeLeavesCountI();
	tree.levelorderTraverseI();
	std::cout << std::endl << std::endl;

	tree.postOrderWithVisitor(printNode<int>);
	std::cout << std::endl << std::endl;

	std::cout << "preorder:   ";
	tree.preorderTraverse();
	std::cout << std::endl;

	std::cout << "preorder I: ";
	tree.preorderTraverseI();
	std::cout << std::endl;

	std::cout << "inoreder:   ";
	tree.inorderTraverse();
	std::cout << std::endl;

	std::cout << "inoreder I: ";
	tree.inorderTraverseI();
	std::cout << std::endl;

	std::cout << "postorder:   ";
	tree.postorderTraverse();
	std::cout << std::endl;

	std::cout << "postorder I: ";
	tree.postorderTraverseI();
	std::cout << std::endl;

	std::cout << "levelorder I: ";
	tree.levelorderTraverseI();
	std::cout << std::endl;

	std::cout << "Zigzag traverse: ";
	tree.zigzagTraverse();
	std::cout << std::endl;

	std::cout << "Width of the tree is " << tree.width() << std::endl;
	std::cout << "Height of the tree is " << tree.height() << std::endl;
	std::cout << "Count of leaves of the tree is " << tree.countOfLeaves() << std::endl;
	std::cout << "Count of leaves of the tree(from iterative function) is " << tree.countOfLeavesI() << std::endl;
	std::cout << "Count of nodes of the tree is " << tree.countOfNodes() << std::endl;
	std::cout << "Count of nodes of the tree(from iterative function) is " << tree.countOfNodesI() << std::endl;
	
	tree.clear();
	std::cout << "Count of nodes of the tree after clear() is " << tree.countOfNodes() << std::endl;
	

	return 0;
}